﻿using ScriptableObjectEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeCount : MonoBehaviour
{
    [SerializeField] IntEvent onTimeElapseUpdate;

    // Update is called once per frame
    void Update()
    {
        onTimeElapseUpdate.Invoke((int)Time.time);
    }
}
