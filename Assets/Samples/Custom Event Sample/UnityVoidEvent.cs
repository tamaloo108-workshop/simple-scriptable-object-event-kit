﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjectEvent
{

    [System.Serializable]
    public class UnityVoidEvent : UnityEvent<Void>
    {


    }
}