﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjectEvent
{
    public abstract class MultipleBaseGameEventListener<T, E, UER> : MonoBehaviour, IGameEventListener<T> where E : BaseGameEvent<T> where UER : UnityEvent<T>
    {
        [SerializeField] List<E> gameEvents;
        [SerializeField] UER unityEventResponse;

        public List<E> GameEvents { get => gameEvents; set => gameEvents = value; }

        private void OnEnable()
        {
            if (gameEvents == null) return;
            
            for (int i = 0; i < GameEvents.Count; i++)
            {
                GameEvents[i].RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (gameEvents == null) return;
            for (int i = GameEvents.Count - 1; i >= 0; i--)
            {
                GameEvents[i].UnregisterListener(this);
            }
        }

        public void OnEventInvoke(T item)
        {
            if (unityEventResponse != null)
            {
                unityEventResponse.Invoke(item);
            }
        }
    }
}
