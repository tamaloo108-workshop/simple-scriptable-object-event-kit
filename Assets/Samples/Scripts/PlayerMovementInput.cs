﻿using ScriptableObjectEvent;
using UnityEngine;


//player based input.
public class PlayerMovementInput : MovementInput
{
    private Vector2 dir;
    public override bool isValidToMove { get => Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0; }

    public PlayerMovementInput(float speed, BaseActorDataHolder onMove) : base(speed, onMove)
    {

    }

    public override void SetMoveInput()
    {
        if (!isValidToMove) return;
        dir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        _movementData = new MovementStruct(dir.x, dir.y, speed, onMove.ActorId);
        onMove.OnActorMove.Invoke(_movementData.Value);
    }
}
