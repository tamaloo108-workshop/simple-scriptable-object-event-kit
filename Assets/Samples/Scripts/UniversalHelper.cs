﻿using ScriptableObjectEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UniversalHelper
{
    static Dictionary<string, Transform> ActorsDict = new Dictionary<string, Transform>();

    static ActorData[] tempData;
    static Transform[] tempTransformData;

    static GameObject tempName;

    public static Transform GetActorById(string id)
    {
        if (ActorsDict == null || !(ActorsDict.ContainsKey(id))) return null;

        return ActorsDict[id];
    }

    public static void RegisterActor(ActorIdData actor, Transform actorTransform)
    {
        if (ActorsDict.ContainsKey(actor.id))
        {
            ActorsDict[actor.id] = actorTransform;
            return;
        }

        ActorsDict.Add(actor.id, actorTransform);
    }

    public static ActorData GetActorData(this Transform myData)
    {
        if (ActorsDict.ContainsValue(myData))
        {
            return myData.GetComponent<NpcActorDataHolder>().ActorData;
        }

        return null;
    }
}
