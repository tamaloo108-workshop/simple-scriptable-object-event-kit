﻿using ScriptableObjectEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    protected Vector3 targetPosition;
    protected Vector3 calculatedTargetPosition;

    public void MoveActor(MovementStruct movementData)
    {
        targetPosition = new Vector3(movementData.dirX, 0f, movementData.dirY);
        calculatedTargetPosition = Vector3.MoveTowards(UniversalHelper.GetActorById(movementData.actorReceiverId).position, UniversalHelper.GetActorById(movementData.actorReceiverId).position + targetPosition, movementData.speedAmount * Time.deltaTime);
        UniversalHelper.GetActorById(movementData.actorReceiverId).position = calculatedTargetPosition;
    }
    
}
