﻿using ScriptableObjectEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//base movement input.
public abstract class MovementInput
{
    protected float speed;
    protected BaseActorDataHolder onMove;
    protected MovementStruct? _movementData;

    public abstract bool isValidToMove { get; }
    public abstract void SetMoveInput();

    protected MovementInput(float speed, BaseActorDataHolder onMove)
    {
        this.speed = speed;
        this.onMove = onMove;
    }
}
