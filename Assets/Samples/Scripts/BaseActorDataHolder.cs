﻿using ScriptableObjectEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseActorDataHolder : MonoBehaviour
{
    [SerializeField] protected ActorIdData actorIdSO;
    [SerializeField] protected ActorData actorDataSO;

    [Header("Actor Events")]
    [SerializeField] protected MovementEvent onActorMove;

    public ActorData ActorData => actorDataSO;
    public string ActorId => actorIdSO.id;
    public MovementEvent OnActorMove => onActorMove;

    protected virtual void Awake()
    {
        UniversalHelper.RegisterActor(actorIdSO, transform);
    }

    protected abstract void Start();
    protected abstract void Update();

}
