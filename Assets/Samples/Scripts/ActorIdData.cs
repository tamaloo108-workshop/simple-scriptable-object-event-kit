﻿using UnityEngine;

namespace ScriptableObjectEvent
{
    [CreateAssetMenu(fileName = "NewActorIdData", menuName = "Scriptable Object Event/Data/Create ActorID Data")]
    public class ActorIdData : ScriptableObject
    {
        public string id;
    }

}