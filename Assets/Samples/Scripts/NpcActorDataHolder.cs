﻿using System.Collections;
using UnityEngine;

public class NpcActorDataHolder : BaseActorDataHolder
{

    NpcMovementInput input;
    private WaitForSeconds UpdateInputDelay;

    protected override void Start()
    {
        UpdateInputDelay = new WaitForSeconds(0.8f);
        input = new NpcMovementInput(actorDataSO.actorData.speed, this, actorDataSO.actorData.isActive, actorDataSO.actorData.xRange, actorDataSO.actorData.yRange);

        StartCoroutine(UpdateInputValue());
    }

    private IEnumerator UpdateInputValue()
    {
        for (; ; )
        {
            input = new NpcMovementInput(actorDataSO.actorData.speed, this, actorDataSO.actorData.isActive, actorDataSO.actorData.xRange, actorDataSO.actorData.yRange);
            yield return UpdateInputDelay;
        }
    }

    protected override void Update()
    {
        input.SetMoveInput();
    }
}
