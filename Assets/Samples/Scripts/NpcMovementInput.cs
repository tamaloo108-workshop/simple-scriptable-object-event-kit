﻿using ScriptableObjectEvent;
using UnityEngine;
//npc based input
public class NpcMovementInput : MovementInput
{
    private Vector2 dir;
    private float xRange;
    private float yRange;
    public bool isActive;
    public override bool isValidToMove { get => isActive; }
    private bool isInverseDir => Random.value > .3f;
    private bool isUpdateMoveDir => Random.value > 1f;


    public NpcMovementInput(float speed, NpcActorDataHolder onMove, bool isActive, float xRange, float yRange) : base(speed, onMove)
    {
        this.xRange = xRange;
        this.yRange = yRange;
        this.isActive = isActive;
    }

    public override void SetMoveInput()
    {
        if (!isValidToMove) return;
        if (!isUpdateMoveDir)
        {
            if (!_movementData.HasValue)
            {
                dir = new Vector2(Random.value * xRange, Random.value * yRange);
                dir = isInverseDir ? -dir : dir;
                _movementData = new MovementStruct(dir.x, dir.y, speed, onMove.ActorId);
            }
            onMove.OnActorMove.Invoke(_movementData.Value);
            return;
        }

        dir = new Vector2(Random.value * xRange, Random.value * yRange);
        dir = isInverseDir ? -dir : dir;
        _movementData = new MovementStruct(dir.x, dir.y, speed, onMove.ActorId);
        onMove.OnActorMove.Invoke(_movementData.Value);
    }
}
