﻿using System.Collections;
using UnityEngine;

public class PlayerActorDataHolder : BaseActorDataHolder
{
    PlayerMovementInput input;
    private WaitForSeconds UpdateInputDelay;

    protected override void Start()
    {
        UpdateInputDelay = new WaitForSeconds(0.8f);
        input = new PlayerMovementInput(actorDataSO.actorData.speed, this);

        StartCoroutine(UpdateInputValue());
    }

    private IEnumerator UpdateInputValue()
    {
        for (; ; )
        {
            input = new PlayerMovementInput(actorDataSO.actorData.speed, this);
            yield return UpdateInputDelay;
        }
    }

    protected override void Update()
    {
        input.SetMoveInput();
    }
}
