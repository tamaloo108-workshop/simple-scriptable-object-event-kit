﻿using UnityEngine;

namespace ScriptableObjectEvent
{

    [CreateAssetMenu(fileName = "NewVoidEvent", menuName = "Scriptable Object Event/Event/Create Movement Event")]
    public class MovementEvent : BaseGameEvent<MovementStruct>
    {
        public void Invoke() => Invoke(new MovementStruct());

    }

}